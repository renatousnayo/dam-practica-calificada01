import React, {Component} from 'react';
import {View, StyleSheet, Text, ScrollView, Image} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

const DATA = [
  {
    id: '1',
    title: 'Iron Man',
    description: 'Tony Stark - es un superhéroe ficticio que aparece en los cómics estadounidenses publicados por Marvel Comics. El personaje fue cocreado por el escritor y editor Stan Lee, desarrollado por el guionista Larry Lieber y diseñado por los artistas Don Heck y Jack Kirby. Hizo su primera aparición en Tales of Suspense # 39 (marzo de 1963), y recibió su propio título en Iron Man #1 (mayo de 1968).',
    picURL: 'https://static.posters.cz/image/750/poster/marvel-iron-man-cover-i13712.jpg'
  },
  {
    id: '2',
    title: 'Capitan America',
    description: 'Steve Rogers - es un superhéroe ficticio que aparece en los cómics estadounidenses publicados por Marvel Comics. Creado por los historietistas Joe Simon y Jack Kirby, el personaje apareció por primera vez en Captain America Comics #1 (marzo de 1941) de Timely Comics, predecesor de Marvel Comics.',
    picURL: 'https://cloud10.todocoleccion.online/comics-vertice/tc/2016/02/18/00/54526177.jpg'
  },
  {
    id: '3',
    title: 'Thor',
    description: 'Hijo de Odin -  es un superhéroe ficticio que aparece en los cómics estadounidenses publicados por Marvel Comics. El personaje, que se basa en la deidad nórdica del mismo nombre, es el dios del trueno asgardiano poseedor del martillo encantado, Mjolnir, que le otorga la capacidad de volar y manipular el clima entre sus otros atributos sobrehumanos.',
    picURL: 'https://i.pinimg.com/originals/04/f8/9c/04f89c3ca0846d602309548ee74fb549.jpg'
  },
  {
    id:'4',
    title:'Viuda Negra',
    description:'Natasha Romanova - es una superheroína ficticia que aparece en el cómic estadounidense y libros publicados por Marvel Comics. Creado por el editor y trazador Stan Lee, el guionista Don Rico y el artista Don Heck, el personaje debutó en Tales of Suspense # 52 (abril de 1964). El personaje fue presentado como una espía rusa, una antagonista del superhéroe Iron Man. ',
    picURL:'https://images-na.ssl-images-amazon.com/images/I/518rnaeEedL._SX328_BO1,204,203,200_.jpg'
  },
  {
    id:'5',
    title:'Vision',
    description:'Vision -  es un androide y superhéroe ficticio de los cómics Marvel Comics, miembro del supergrupo de los Vengadores. Apareció por primera vez en The Avengers # 57 (octubre de 1968). Está basado libremente en el personaje de Timely Comics del mismo nombre.',
    picURL:'https://img.vixdata.io/pd/jpg-large/es/sites/default/files/btg/comics.batanga.com/files/La-portada-del-dia-Avengers-57-2.jpg'
  },
  {
    id:'6',
    title:'Hulk',
    description:'Robert Bruce Banner - es un personaje ficticio, un superhéroe que aparece en los cómics estadounidenses publicados por la editorial Marvel Comics. El personaje fue creado por los escritores Stan Lee y Jack Kirby siendo su primera aparición en The Incredible Hulk #1 publicado en mayo de 1962.',
    picURL:'https://images-na.ssl-images-amazon.com/images/S/cmx-images-prod/Item/691507/691507._SX360_QL80_TTD_.jpg'
  },

];

function Item({title,image}) {
  return (
    <View style={styles.item}>
      <Image style={styles.image} source={{uri: image}}/>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

export default class OurFlatList extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (
      <View style={styles.container}>
        {}
        <ScrollView>
          {DATA.map(item => (
            <TouchableOpacity style={styles.item} onPress={()=> this.props.navigation.navigate('Details',{
              itemID: item.id,
              itemTitle: item.title,
              itemDesc: item.description,
              itemPic: item.picURL})}>
              
              <Item title={item.title} image={item.picURL}/>
              
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#212121'
  },
  item: {
    flexDirection: 'row',
    backgroundColor: '#0277bd',
    padding: 1,
    marginVertical: 5,
    marginHorizontal: 5
  },
  title: {
    fontSize: 32,
    textAlignVertical: "center",
    marginStart: 15,
    color: 'white'
  },
  image:{
    height: 80,
    width: 80,
    borderColor: '#212121',
    borderWidth: 5,
    padding: 15
  }
});
